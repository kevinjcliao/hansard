defmodule Hansard.SpeechController do
  use Hansard.Web, :controller

  alias Hansard.Speech

  def index(conn, _params) do
    speeches = Enum.reverse(Repo.all(Speech))
    changeset = Speech.changeset(%Speech{})
    render(conn, "index.html", speeches: speeches, changeset: changeset)
  end

  def new(conn, _params) do
    changeset = Speech.changeset(%Speech{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"speech" => speech_params}) do
    changeset = Speech.changeset(%Speech{}, speech_params)

    case Repo.insert(changeset) do
      {:ok, speech} ->
        # Send speech to all currently connected clients.
        Hansard.SpeechChannel.broadcast_change(speech)
        # Remove Redirect.
        conn
        |> put_flash(:info, "Speech created successfully.")
        |> redirect(to: speech_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    speech = Repo.get!(Speech, id)
    render(conn, "show.html", speech: speech)
  end

  def edit(conn, %{"id" => id}) do
    speech = Repo.get!(Speech, id)
    changeset = Speech.changeset(speech)
    render(conn, "edit.html", speech: speech, changeset: changeset)
  end

  def update(conn, %{"id" => id, "speech" => speech_params}) do
    speech = Repo.get!(Speech, id)
    changeset = Speech.changeset(speech, speech_params)

    case Repo.update(changeset) do
      {:ok, speech} ->
        conn
        |> put_flash(:info, "Speech updated successfully.")
        |> redirect(to: speech_path(conn, :show, speech))
      {:error, changeset} ->
        render(conn, "edit.html", speech: speech, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    speech = Repo.get!(Speech, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(speech)

    conn
    |> put_flash(:info, "Speech deleted successfully.")
    |> redirect(to: speech_path(conn, :index))
  end
end
