defmodule Hansard.Router do
  use Hansard.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Hansard do
    pipe_through :browser # Use the default browser stack

    # get "/", PageController, :index
    resources "/", SpeechController
  end

  scope "/auth", Hansard do
    pipe_through :browser

    get "/:provider", AuthController, :index
    get "/:provider/callback", AuthController, :callback
  end
  # Other scopes may use custom stacks.
  # scope "/api", Hansard do
  #   pipe_through :api
  # end
end
