import socket from './socket';

console.log("Speech.js was loaded.");
$(function() {
  console.log("This function is running.");
  let ul = $("tbody#show-list");
  let id = ul.data("id");
  let topic = "speech:" + id;

  console.log("Joining topic: ", topic);
  // Join the topic
  let channel = socket.channel(topic, {});
  channel.join()
    .receive("ok", data => {
      console.log("Joined topic: ", topic);
    })
    .receive("error", resp => {
      console.log("Error. Unable to join topic: ", topic, resp);
    });

  channel.on("new", speech => {
    console.log("New speech created: ", speech);
    console.log("Prepending this speech to: ");
    ul.prepend(makeSpeech(speech));
  });
});

function makeSpeech(speech) {
  console.log("Making a speech out of: ", speech);
  const keys = ["speaker", "class_year", "inserted_at", "description"];
  let newSpeech = document.createElement("tr");
  let td = "";
  for(var index = 0; index < keys.length; index++) {
    const key = keys[index];
    console.log("Iterating through: ", key);
    td = document.createElement("td");
    td.append(speech[key]);
    newSpeech.append(td);
  }
  newSpeech.append(makeButtons(speech.id));
  return newSpeech;
}

function makeButtons(id) {
  let td = document.createElement("td");
  td.className = "text-right";
  td.append(makeShowButton(id));
  td.append(makeEditButton(id));

  return td;
}

function makeShowButton(id) {
  let a = document.createElement("a");
  a.className = "btn btn-default btn-xs";
  a.href = "/" + id;
  a.append("Show");
  return a;
}

function makeEditButton(id) {
  let a = document.createElement("a");
  a.className = "btn btn-default btn-xs";
  a.href = "/" + id + "/edit";
  a.append("Edit");
  return a;
}
