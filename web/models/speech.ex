defmodule Hansard.Speech do
  use Hansard.Web, :model

  schema "speeches" do
    field :speaker, :string
    field :description, :string
    field :class_year, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:speaker, :description, :class_year])
    |> validate_required([:speaker, :description, :class_year])
  end
end
