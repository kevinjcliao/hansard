defmodule Hansard.SpeechChannel do
  use Hansard.Web, :channel

  def broadcast_change(speech) do
    payload = %{
      "id" => speech.id,
      "speaker" => speech.speaker,
      "description" => speech.description,
      "class_year" => speech.class_year,
      "inserted_at" => speech.inserted_at
    }

    Hansard.Endpoint.broadcast("speech:lobby", "new", payload)
    IO.puts "broadcasted change with payload: "
  end

  def join("speech:lobby", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (speech:lobby).
  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
