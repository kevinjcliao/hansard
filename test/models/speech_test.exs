defmodule Hansard.SpeechTest do
  use Hansard.ModelCase

  alias Hansard.Speech

  @valid_attrs %{class_year: 42, description: "some content", speaker: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Speech.changeset(%Speech{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Speech.changeset(%Speech{}, @invalid_attrs)
    refute changeset.valid?
  end
end
