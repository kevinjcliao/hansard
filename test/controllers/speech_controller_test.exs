defmodule Hansard.SpeechControllerTest do
  use Hansard.ConnCase

  alias Hansard.Speech
  @valid_attrs %{class_year: 42, description: "some content", speaker: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, speech_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing speeches"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, speech_path(conn, :new)
    assert html_response(conn, 200) =~ "New speech"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, speech_path(conn, :create), speech: @valid_attrs
    assert redirected_to(conn) == speech_path(conn, :index)
    assert Repo.get_by(Speech, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, speech_path(conn, :create), speech: @invalid_attrs
    assert html_response(conn, 200) =~ "New speech"
  end

  test "shows chosen resource", %{conn: conn} do
    speech = Repo.insert! %Speech{}
    conn = get conn, speech_path(conn, :show, speech)
    assert html_response(conn, 200) =~ "Show speech"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, speech_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    speech = Repo.insert! %Speech{}
    conn = get conn, speech_path(conn, :edit, speech)
    assert html_response(conn, 200) =~ "Edit speech"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    speech = Repo.insert! %Speech{}
    conn = put conn, speech_path(conn, :update, speech), speech: @valid_attrs
    assert redirected_to(conn) == speech_path(conn, :show, speech)
    assert Repo.get_by(Speech, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    speech = Repo.insert! %Speech{}
    conn = put conn, speech_path(conn, :update, speech), speech: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit speech"
  end

  test "deletes chosen resource", %{conn: conn} do
    speech = Repo.insert! %Speech{}
    conn = delete conn, speech_path(conn, :delete, speech)
    assert redirected_to(conn) == speech_path(conn, :index)
    refute Repo.get(Speech, speech.id)
  end
end
