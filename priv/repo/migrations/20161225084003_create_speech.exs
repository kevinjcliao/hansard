defmodule Hansard.Repo.Migrations.CreateSpeech do
  use Ecto.Migration

  def change do
    create table(:speeches) do
      add :speaker, :string
      add :description, :text
      add :class_year, :integer

      timestamps()
    end

  end
end
